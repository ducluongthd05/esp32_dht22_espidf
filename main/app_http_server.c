#include "app_http_server.h"
#include <esp_http_server.h>
#include "esp_log.h"
static const char *TAG = "app_http_server";
static  httpd_handle_t server = NULL;

extern const uint8_t web_server_start[] asm("_binary_web_server_html_start");
extern const uint8_t web_server_end[] asm("_binary_web_server_html_end");
static http_server_handle http_get_handle = NULL;
static http_server_handle http_post_handle = NULL;
/* An HTTP GET handler */
static esp_err_t hello_get_handler(httpd_req_t *req)
{
    //char*  buf = "Hello World!";
    //size_t buf_len = strlen(buf);
   
    /* Send response with custom headers and body set as the
     * string passed in user context*/
    
    //httpd_resp_set_type(req, "image/jpg");
    httpd_resp_send(req,(const char*) web_server_start, web_server_end - web_server_start);
    return ESP_OK;
}
static const httpd_uri_t hello = {
    .uri       = "/hello",
    .method    = HTTP_GET,
    .handler   = hello_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = "Hello World!"
};
static httpd_req_t *get_req;
void http_send_response(char *data, int len){
    httpd_resp_send(get_req,(const char*) data, len);
}
static esp_err_t dht22_get_handler(httpd_req_t *req)
{
    get_req = req;
    http_get_handle("dht22",5);
    return ESP_OK;
}
static const httpd_uri_t dht22 = {
    .uri       = "/dht22",
    .method    = HTTP_GET,
    .handler   = dht22_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = "dht22"
};
static esp_err_t sw1_get_handler(httpd_req_t *req)
{
    char buf[100];
    int len = httpd_req_recv(req, buf, 100);
    http_post_handle(buf,len);
    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}
static const httpd_uri_t sw1 = {
    .uri       = "/sw1",
    .method    = HTTP_POST,
    .handler   = sw1_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = "sw1"
};
void start_webserver(void)
{
   
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &hello);
        httpd_register_uri_handler(server, &dht22);
        httpd_register_uri_handler(server, &sw1);
    }else{
    ESP_LOGI(TAG, "Error starting server!");
    }
}
void stop_webserver(void)
{
    // Stop the httpd server
    httpd_stop(server);
}
void http_get_set_callback(void *cb){
    if(cb){
        http_get_handle = cb;
    }
}
void http_post_set_callback(void *cb){
    if(cb){
        http_post_handle = cb;
    }
}